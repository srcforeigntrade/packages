from __future__ import print_function
import os
import re
import datetime
import subprocess
import tempfile
import shutil
import xlsxwriter

# This matches URLs.  We use it because each category in a package page is listed as a URL.
cat_pattern = re.compile("\[([\w\s-]+)\]")

# This matches Label: key=value strings found in the package descriptions.
lbl_pattern = re.compile("Label:\s*(?P<key>\w+)\s*=\s*(?P<value>\w+)")

url_pattern = re.compile("(?P<url>http(s)?:\/\/([^\s\]])+)")

# The prefix at the start of a line in a package page that is followed by a list of category URLs
CATEGORY_PREFIX = "Categories:"

# The prefix at the start of a line in a package page that is followed by a URL.
URL_PREFIX = "URL:"
SRC_PREFIX = "Source URL:"

# The directory containing the packages that we are scanning
PACKAGE_SUBDIR = "packages"

# The directory into which we will place the package lists
CATEGORY_SUBDIR = "categories"

# The file that will contain the index page
INDEX_FILE = "home.md"

# The generated Microsoft Excel summary
EXCEL_FILE = "summary.xlsx"

# The number of most recently modified packages to report on
RECENT_PACKAGE_COUNT = 10

GITLAB_PACKAGE_URL_PREFIX = "https://gitlab.com/arm-hpc/packages/wikis/packages/"

GITLAB_CATEGORY_URL_PREFIX = "https://gitlab.com/arm-hpc/packages/wikis/categories/"

# JDW 13/10/17 - default list of categories to be used to generate worksheets in the excel spreadsheet.
#                can be overridden by list hidden in the INDEX_FILE
CATEGORY_SET = ("Openhpc","Isambard-list","Riken-list","Mantevo","Application")

# JDW 13/10/17 - variable to contain category set used to generate excel spreadsheet tabs.
#                Can be set from a hidden tag in the INDEX_FILE, otherwise uses CATEGORY_SET
categorySet = set()

# JDW 16/10/17 as above but for labels (label=value where both are used case insensitive)
WORKSHEET_LABEL_SET = list()
labelList = list()
invisibleLabelList = list()

class Package():

    def __init__(self, filepath):
        self.filename = os.path.basename(filepath)
        self.name = os.path.splitext(self.filename)[0]

        # Obtain date of the last git commit
        last_commit = subprocess.check_output(['git', 'log', '-1', '--format=%ct', '--', filepath])
        try:
            self.last_modified = datetime.datetime.fromtimestamp(int(last_commit))
        except ValueError:
            self.last_modified = datetime.datetime.now()

        self.labels = {}
        self.category_names = set()
        self.category_names_lower = set()
        self.external_url = None
        self.external_src = None

        # Scan the wiki text for the 'Categories' line, and if we find it, record each category that this package
        # belongs to.  Also look for 'Label:' lines to build our set of labels.
        with open(filepath,'r') as package_file:
            for line in package_file:
                # If the line starts with the CATEGORY_PREFIX, all links on that line are Categories
                if line.startswith(CATEGORY_PREFIX):
                    category_string = line[len(CATEGORY_PREFIX):]
                    for cat in cat_pattern.findall(category_string):
                        #print("Found Cat |"+cat+"|")
                        # Remove any accidental whitespace
                        self.category_names.add(cat.strip())
                        self.category_names_lower.add(cat.strip().lower())
                        #print("Added Cat |"+cat.strip()+"|")

                if line.startswith(URL_PREFIX):
                    # Remove the prefix, strip whitespace, split on whitespace or ] to get first word,
                    # Remove leading/trailing [] in case it is in Wiki markup.
                    url_string = line[len(URL_PREFIX):]
                    url_match = url_pattern.search(url_string)
                    if url_match:
                        self.external_url = url_match.group('url')
                # NJS 19/10/17 get the url of the src repo
                if line.startswith(SRC_PREFIX):
                    # Remove the prefix, strip whitespace, split on whitespace or ] to get first word,
                    # Remove leading/trailing [] in case it is in Wiki markup.
                    url_string = line[len(SRC_PREFIX):]
                    url_match = url_pattern.search(url_string)
                    if url_match:
                        self.external_src = url_match.group('url')

                # There can be multiple Label: key=value instances on each line
                iterator = lbl_pattern.finditer(line)
                for match in iterator:
                    self.labels[match.group('key')] = match.group('value')

    def get_label_set(self):
        return set(self.labels.keys())

    def get_label(self, label):
        return self.labels.get(label, None)

    # JDW 13/10/17 - added function to allow lowercase keys to match
    def get_label_lower(self, inLabel):
        for key in self.labels:
            if inLabel == key.lower():
                return self.labels.get(key)
        return  None

    def modified_date_string(self):
        return self.last_modified.strftime("%Y-%m-%d")

    def to_link(self):
        return "[{package}](/{package_subdir}/{package})".format(package=self.name, package_subdir=PACKAGE_SUBDIR)

    def to_url(self):
        return "{prefix}{package}".format(prefix=GITLAB_PACKAGE_URL_PREFIX, package=self.name)

    def get_alphabetical_categories(self):
        return sorted(self.category_names)

    def get_alphabetical_categories_lower(self):
        return sorted(self.category_names_lower)

class Category():

    def __init__(self, name):
        self.name = name.lower()
        self.label = name   # in case the Case is different (names are forced into lower case)
        self.packages = set()

    def add_package(self, package):
        self.packages.add(package)

    def to_link(self):
        #return "[{cat}](/{category_subdir}/{cat})".format(cat=self.name, category_subdir=CATEGORY_SUBDIR)
        #return Category.format_to_link_ref(self.name,self.name)
        return Category.format_to_link_ref(self.label,self.name)

    def get_alphabetical_packages(self):
        return sorted(self.packages, cmp=lambda a,b: cmp(a.name.lower(), b.name.lower()))

    def to_url(self):
          return "{prefix}{cat}".format(prefix=GITLAB_CATEGORY_URL_PREFIX, cat=self.name)

    # JDW 13/10/17 - a static method to generate the same formatting as used by actual catagories
    @staticmethod
    def format_to_link(name):
        #return "[{cat}](/{category_subdir}/{cat})".format(cat=name, category_subdir=CATEGORY_SUBDIR)
        return Category.format_to_link_ref(name,name)

    # JDW 13/10/17 - a static method to generate the same formatting as used by actual catagories
    @staticmethod
    def format_to_link_ref(ref,name):
        return "[{ref}](/{category_subdir}/{cat})".format(ref=ref, cat=name, category_subdir=CATEGORY_SUBDIR)

# JDW 16/10/17 - inStr contains a key=value pair for a label to be used to generate worksheet tabs but need to
#                to be split. Should return a set of 2 but caller must check
def splitLabelStr(inStr):
    tmpList = list()
    for entry in inStr.split("="):
        if not entry.isspace():
            tmpList.append(entry)
    return tmpList

# JDW 05/02/18 - inStr contains a list of labels to be used to generate worksheet columns but need to
#                to be extracted
# JDW 16/10/17 - inStr contains a list of label=values to be used to generate worksheet tabs but need to
#                to be extracted
# JDW 05/02/18 - tried to add robustness for users failing to properly leave spaces between html comment tags
def extractLabelSet(inStr):
    if (inStr.startswith("<!-- CATEGORY_SET")) or (inStr.startswith("<!--CATEGORY_SET")):
        for entry in inStr.split(" "):
            entry = entry.rstrip("\n")
            # If the user didn't leave a space between the label name the closing html comment tag
            if (entry.endswith("-->")):
                entry = entry.rstrip("-->")
            # If the user didn't leave a space between the html name the opening html comment tag
            if (entry.startswith("<!--")):
                continue
            # An empty string
            if (len(entry) == 0):
                return
            if entry == "<!--" or entry == "CATEGORY_SET" or "-->" in entry:
                continue
            categorySet.add(entry)
        return

    if (inStr.startswith("<!-- WORKSHEET_LABEL_SET")) or (inStr.startswith("<!--WORKSHEET_LABEL_SET")):
        for entry in inStr.split(" "):
            entry = entry.rstrip("\n")
            # If the user didn't leave a space between the label name the closing html comment tag
            if (entry.endswith("-->")):
                entry = entry.rstrip("-->")
            # If the user didn't leave a space between the html name the opening html comment tag
            if (entry.startswith("<!--")):
                continue
            # An empty string
            if (len(entry) == 0):
                return
            if entry == "<!--" or entry == "WORKSHEET_LABEL_SET" or "-->" in entry:
                continue
            labelList.append(entry)
        return

    if (inStr.startswith("<!-- INVISIBLE_LABEL_SET")) or (inStr.startswith("<!--INVISIBLE_LABEL_SET")):
        for entry in inStr.split(" "):
            entry = entry.rstrip("\n")
            # If the user didn't leave a space between the label name the closing html comment tag
            if (entry.endswith("-->")):
                entry = entry.rstrip("-->")
            # If the user didn't leave a space between the html name the opening html comment tag
            if (entry.startswith("<!--")):
                continue
            # An empty string
            if (len(entry) == 0):
                return
            # Correctly formatted html comment tage
            if entry == "<!--" or entry == "INVISIBLE_LABEL_SET" or "-->" in entry:
                continue
            invisibleLabelList.append(entry.lower())
        return

# JDW 05/02/18 - extracted this to a function of its own as we need the data from INDEXFILE
#                to control some aspects of the proposed contents of the file which are
#                generated before the replace_into function is ready to be called
#              - Also, tried to limit user cack-handedness by missing space after HTML comment tag
def get_user_control_info(filename ):

    if os.path.isfile(filename):
        with open(filename, 'r') as orig_file:
            for line in orig_file:
                if (line.startswith("<!-- CATEGORY_SET")) or (line.startswith("<!--CATEGORY_SET")):
                    extractLabelSet(line)
                if (line.startswith("<!-- WORKSHEET_LABEL_SET")) or (line.startswith("<!--WORKSHEET_LABEL_SET")):
                    extractLabelSet(line)
                if (line.startswith("<!-- INVISIBLE_LABEL_SET")) or (line.startswith("<!--INVISIBLE_LABEL_SET")):
                    extractLabelSet(line)
            orig_file.close()

# If the file does not exist, creates it with the specified content between
# special comment markers and performs a 'git add'.
# If the file already exists, search it for those comment markers and replace
# what is inside them with the specified content.
def replace_into(filename, content):
    # Ensure that content ends with newline
    if not content.endswith("\n"):
        content += "\n"

    if os.path.isfile(filename):
        print("Not overwriting {}".format(filename))

        with open(filename, 'r') as orig_file:
            tmp = tempfile.NamedTemporaryFile(delete=False, mode='w')
            copyLine = True

            for line in orig_file:
                if copyLine:
                    tmp.write(line)

                # JDW 13/10/17 - check for line that contains list of categories that
                #                controls spreadsheet generation (only in home.md)
                #                similarly for label sets
#                if line.startswith("<!-- CATEGORY_SET"):
#                    extractCategorySet(line)
#                if line.startswith("<!-- WORKSHEET_LABEL_SET"):
#                    extractLabelSet(line)
#                if line.startswith("<!-- VISIBLE_LABEL_SET"):
#                    extractLabelSet(line)
                if line.startswith("<!-- BEGIN AUTOGEN -->"):
                    copyLine = False;
                    tmp.write(content)
                elif line.startswith("<!-- END AUTOGEN -->"):
                    tmp.write(line)
                    copyLine = True;

        tmp.close()
        shutil.copy(tmp.name, filename)
        os.remove(tmp.name)
    else:
        # File does not exist, generate it.
        print("Generating {}".format(filename))
        with open(filename, 'w') as output_file:
            output_file.write("<!-- BEGIN AUTOGEN -->\n")
            output_file.write(content)
            output_file.write("<!-- END AUTOGEN -->\n")
        git_add_result = subprocess.check_output(['git', 'add', filename])
        print(git_add_result)

def render_markdown_table(row_keys, row_renderer):
    # Given a list of row_keys, generates a table by repeatedly calling the
    # row_renderer.get_cells method, passing in each key in turn.  The
    # results of each call are a list of cell values.  This function
    # turns this into a table, one row per row_key, with the header names
    # generated by calling row_renderer.header_list.
    def key_to_row(key):
        return "| {} |".format(" | ".join(row_renderer.get_cells(key)))

    return "| {headers} | \n| {spacers} |\n{body}".format(headers=" | ".join(row_renderer.header_list),
                                                     spacers=" | ".join(["---"] * len(row_renderer.header_list)),
                                                     body="\n".join(map(key_to_row, row_keys)))

class CategoryTableRenderer():
    # This is a row_renderer, compatible with the render_markdown_table
    # function above.  It is used to generate the summaries of each
    # package in a given category - columns are package name, date of
    # last modification, and all labels.
    def __init__(self, labels):
        self.labels = labels
        self.header_list = ["package", "last modified"] + self.labels

    def get_label_text(self, package, label):
        # jdw 2/2/18 - requested to filter neonoptimised from table
        if (label == "neonoptimized"):
            return
        l = package.get_label(label)
        if l is None:
            return "<span style=\"color:#c0c0c0\">-</span>"
        elif l == "yes":
            return "<span style=\"color:green\">yes</span>"
        elif l == "supported":
            return "<span style=\"color:green\">supported</span>"
        elif l == "needspatch":
            return "<span style=\"color:green\">needspatch</span>"
        else:
            return l

    def get_cells(self, package):
        return [package.to_link(), package.modified_date_string()] + map(lambda l: self.get_label_text(package, l), self.labels)

# function to add rows to an already generated worksheet based on a packages category and labels.
# special value of "ARM_All" can be used for either/both of category/label
# if labelKey is not ARM_All then we look at labelValue, otherwise we ignore it
def AddToWorkSheet(worksheet,categoryToAdd,labelKeyToAdd,labelValueToAdd="ARM_All"):

    print("AddToWorkSheet "+categoryToAdd+" "+labelKeyToAdd+" "+labelValueToAdd)

    # Generate the header row
    worksheet.set_row(0, None, fmt_heading)
    worksheet.write(0, 0,"Package")
    worksheet.write(0, 1,"External URL")
    worksheet.write(0, 2,"Last Wiki Update")

    for index, label in enumerate(label_list):
        worksheet.write(0,3+index, label, fmt_label_header)

    # We don't generate the "Categories" header until later on, once we know how many cells it must span.

    # Set the column widths
    worksheet.set_column(0,0,20)
    worksheet.set_column(1,1,50)
    worksheet.set_column(2,2,20)
    worksheet.set_column(3,2+len(label_list),10)
    worksheet.set_column(3+len(label_list),2 + len(label_list) + len(category_list),20)

    # We track the largest number of categories we've found, so we can size the
    # 'Categories' header cell appropriately
    max_categories = 0

    # JDW 12/10/17 - since we can now skip rows based on which categories we are putting into the
    #                worksheet, need a row cntr to replace index
    rowIndx = 0

    # Generate one row per package
    for  package in package_list:

        # JDW 12/10/17 - compare categoryToAdd to either "all" or the category names from the package
	# JDW 23/01/18 - package.get_alphabetical_categories to package.get_alphabetical_categories_lower as no
	#                no matches were getting made (obviously) and the worksheets are empty
        if categoryToAdd != 'ARM_All' and not categoryToAdd.lower() in package.get_alphabetical_categories_lower():
            continue

        # JDW 12/10/17 - compare categoryToAdd to either "all" or the labels from the package
        if labelKeyToAdd != 'ARM_All' and \
            not labelKeyToAdd.lower() in [x.lower() for x in package.get_label_set()]:
            continue

        # JDW 12/10/17 - if we have a specific label, we may also care about its specific value.
        if labelKeyToAdd != 'ARM_All':
            if labelValueToAdd != 'ARM_All' and \
                labelValueToAdd.lower() != package.get_label_lower(labelKeyToAdd.lower()).lower():
                continue

        # Name of package, linked back to the Wiki
        worksheet.write_url(rowIndx+1,0,package.to_url(), fmt_url, package.name)

        # External URL, if one exists
        # NJS 19/10/17. If there's no URL use the SRC location if given.
        if package.external_url:
            worksheet.write_url(rowIndx+1,1,package.external_url, fmt_url)
        elif package.external_src:
            worksheet.write_url(rowIndx+1,1,package.external_src, fmt_url)

        # Date Wiki page was last modified
        worksheet.write_datetime(rowIndx+1,2, package.last_modified, fmt_date)

        # Labels.  Anything with "Yes" as the value appears in a different format
        for lbl_index, label in enumerate(label_list):
            label_text = package.get_label(label)

            if label_text is None:
                label_text = "-"
                fmt = fmt_label_missing
            elif label_text in ["Yes", "Upstream", "Supported", "NA"]:
                fmt = fmt_label_good
            elif label_text in ["NeedsPatch"]:
                fmt = fmt_label_amber
            else:
                fmt = fmt_label

            worksheet.write(rowIndx+1,3 + lbl_index, label_text, fmt)

        # Categories, with links back to the Wiki.
        package_cats = package.get_alphabetical_categories_lower()

        # Update the longest number of categories we've found
        max_categories = max(max_categories, len(package_cats))

        for cat_index, category in enumerate(package_cats):
            worksheet.write_url(rowIndx+1, 3 + len(label_list) + cat_index, categories[category].to_url(), fmt_url, categories[category].label)

        # JDW 12/10/17 - now need to increment row cntr
        rowIndx += 1

    # We generate one cell per category, so make this header merge across them all.
    if max_categories == 1:
        worksheet.write(0, 3+len(label_list), "Categories")
    else:
        worksheet.merge_range(0, 3 + len(label_list),
                              0, 2 + len(label_list) + max_categories,
                              "Categories")

# Map of category slugs to category objects
categories = {}

# Set of packages
packages = set()

# Set of all labels that we encountered during our scan of the packages
labels = set()

# ----

# JDW 04/02/18 - Read INDEX_FILE to get user control info
get_user_control_info(INDEX_FILE)

# Read every file in the dirname, building up a map of categories to pages
for filename in os.listdir(PACKAGE_SUBDIR):
    pathname = os.path.join(PACKAGE_SUBDIR, filename)

    # Ignore anything that is a directory or doesn't end with .md
    if os.path.isdir(pathname) or not filename.endswith(".md"):
        continue

    package = Package(pathname)
    packages.add(package)

    # Update our mapping of categories to packages
    for category_name in package.category_names:
        #JDW 3/12/17 - force into lower case to compensate for users typing category names with
        #              different captitalisation and generating extra category files
        category = categories.get(category_name.lower())
        if not category:
            # Pass the original text to the Category constructor, it creates a lowercase version
            # internaly called name and keeps the original capitalisation (if any) as label
            category = Category(category_name)
            # Use the lowercase name as the key in the categories set
            categories[category_name.lower()] = category
        category.add_package(package)

    # Maintain a list of all the labels we've encountered
    labels.update(package.get_label_set())

# Sort the labels we found into alphabetical order
label_list = sorted(labels)

# JDw 5/2/18 - requested to remove NEONOptimised from table on web page and spreadsheet. Have made it more general,
#              there is a user defined list on the INDEX_FILE called INVISIBLE_LABEL_SET these labels are filtered 
#              from label_set
visibleLabelList = list();
for label in label_list:
    if label.lower() not in invisibleLabelList:
        visibleLabelList.append(label);
label_list = visibleLabelList

# Generate the category pages
render = CategoryTableRenderer(label_list)

for cat in categories.values():
    cat_path = os.path.join(CATEGORY_SUBDIR, cat.name + ".md")

    cat_content = ("## Packages in the '{cat}' category\n\n{cat_table}".format(
                    cat=cat.label,
                    cat_table=render_markdown_table(cat.get_alphabetical_packages(), render)))

    replace_into(cat_path, cat_content)
    #print("Label = |"+cat.label+"|");
    #print(cat_content)

# JDW 13/10/17 - generate an allPackages list in categories so we can remove the full table from the front page
# JDW 15/12/17 - fix sorting to be irrespective or caps
package_list = sorted(packages, cmp=lambda a,b: cmp(a.name.lower(), b.name.lower()))
all_content = ("## All Packages\n\n{cat_table}".format(
                        cat_table=render_markdown_table(package_list, render)))
all_path=os.path.join(CATEGORY_SUBDIR, "allPackages.md")
replace_into(all_path, all_content)

# Generate the main index file
category_list = sorted(categories.values(), cmp=lambda a,b: cmp(a.name, b.name))
category_list_string = ", ".join( map( lambda x: x.to_link(), category_list))
# JDW 13/10/17 - create a link manually for the (not real) allPackages category and put it at the front
#                of the category_list_string
all_list_string = Category.format_to_link("allPackages")
category_list_string = "**"+all_list_string+"**, "+category_list_string

# Generate links to the most recently modified packages
def package_date_cmp(a,b):
    return cmp(b.last_modified, a.last_modified)
date_sorted_packages = sorted(packages, cmp = package_date_cmp)[:RECENT_PACKAGE_COUNT]
date_sorted_packages_string = ",".join( map(lambda x: x.to_link(), date_sorted_packages))

package_list = sorted(packages, cmp=lambda a,b: cmp(a.name, b.name))
#package_table = render_markdown_table(package_list, render)
package_table = render_markdown_table(date_sorted_packages, render)
all_list_string = Category.format_to_link_ref("here","allPackages")

#index_content = ("## Categories\n{cat}\n" + \
#                 "## Most recently modified\n{recently_modified}\n" + \
#                 "## All Packages\n{package_table}\n").format(cat=category_list_string,
#                 recently_modified=date_sorted_packages_string,
#                 package_table=package_table)
index_content = ("## All Packages\n A list of all packages can be found here {allPackagesLnk}\n"
                 "## Categories\n{cat}\n"
                 "## Most recently modified packages\n{package_table}\n").format(
                     cat=category_list_string,
                     package_table=package_table,
                     allPackagesLnk=all_list_string)

replace_into(INDEX_FILE, index_content)

### Generate the Excel Spreadsheet
print("Generating Excel Spreadsheet: " + EXCEL_FILE)
workbook = xlsxwriter.Workbook(EXCEL_FILE)
# The various cell formats that we use
fmt_heading = workbook.add_format({'bold': True})
fmt_url = workbook.add_format({'font_color': 'blue', 'underline': 1})
fmt_date = workbook.add_format({'num_format': 'dd/mm/yy hh:mm:ss', 'align': 'left'})
fmt_label_header = workbook.add_format({'bold': True, 'rotation': 90, 'align': 'center'})
fmt_label = workbook.add_format({'align': 'center'})
fmt_label_good = workbook.add_format({'align': 'center', 'bg_color':'green', 'font_color':'white'})
fmt_label_amber = workbook.add_format({'align': 'center', 'bg_color':'orange', 'font_color':'white'})
fmt_label_missing = workbook.add_format({'align': 'center', 'font_color':'silver'})

#Add a worksheet of all packages
worksheet = workbook.add_worksheet("All Packages")
categoryToAdd="ARM_All"
labelToAdd="ARM_All"
AddToWorkSheet(worksheet,categoryToAdd,labelToAdd)

#Add additional worksheets for set of categories

# JDW 11/10/17 - all categories? Seems too many
#categorySet = set()
#for package in package_list:
#    categorySet.update(package.get_alphabetical_categories())

# JDW 11/10/17 - selected list
if len(categorySet) == 0:
    categorySet = CATEGORY_SET
labelToAdd="ARM_All"
for categoryToAdd in sorted(categorySet):
    worksheet = workbook.add_worksheet(categoryToAdd)
    AddToWorkSheet(worksheet,categoryToAdd,labelToAdd)

# JDW 16/10/17 - Add additional worksheets for labels
categoryToAdd="ARM_All"
if len(labelList) == 0:
    labelList = WORKSHEET_LABEL_SET
if len(labelList) != 0:
    for labelToAdd in labelList:
        labelToAddList = splitLabelStr(labelToAdd)
        # check was valid key=value and also is a valid label before creating worksheet
        if len(labelToAddList) == 2 and labelToAddList[0].lower() in [x.lower() for x in labels]:
            worksheet = workbook.add_worksheet(labelToAddList[0])
            AddToWorkSheet(worksheet,categoryToAdd,labelToAddList[0],labelToAddList[1])

#add two more sheets based om compiles with ARM/GCC
#worksheet = workbook.add_worksheet("ARM_Compiler")
#categoryToAdd="ARM_All"
#labelToAdd="compilesarmcompiler"
#AddToWorkSheet(worksheet,categoryToAdd,labelToAdd,"yes")
#worksheet = workbook.add_worksheet("GCC_Compiler")
#categoryToAdd="ARM_All"
#labelToAdd="compilesgcc"
#AddToWorkSheet(worksheet,categoryToAdd,labelToAdd,"yes")

#close the workbook
workbook.close()
